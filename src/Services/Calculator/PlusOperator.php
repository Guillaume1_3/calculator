<?php

namespace App\Services\Calculator;

class PlusOperator implements OperatorInterface
{
    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return 'plus';
    }

    /**
     * @inheritDoc
     */
    public function calculate(float $firstNumber, float $secondNumber): float
    {
        return $firstNumber + $secondNumber;
    }
}
