<?php

namespace App\Services\Calculator;

class MultiplicationOperator implements OperatorInterface
{
    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return 'multiplication';
    }

    /**
     * @inheritDoc
     */
    public function calculate(float $firstNumber, float $secondNumber): float
    {
        return $firstNumber * $secondNumber;
    }
}
