<?php

namespace App\Services\Calculator;

use App\Exception\Calculator\CalculationException;

interface OperatorInterface
{
    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @param float $firstNumber
     * @param float $secondNumber
     *
     * @return float
     *
     * @throws CalculationException
     */
    public function calculate(float $firstNumber, float $secondNumber): float;
}
