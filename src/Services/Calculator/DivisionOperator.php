<?php

namespace App\Services\Calculator;

use App\Exception\Calculator\DivideByZeroCalculationException;

class DivisionOperator implements OperatorInterface
{
    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return 'division';
    }

    /**
     * @inheritDoc
     */
    public function calculate(float $firstNumber, float $secondNumber): float
    {
        if (0 == $secondNumber) {
            throw new DivideByZeroCalculationException();
        }

        return $firstNumber / $secondNumber;
    }
}
