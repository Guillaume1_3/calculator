<?php

namespace App\Services\Calculator;

use App\Exception\Calculator\CalculationException;
use App\Exception\Calculator\OperatorNotFoundException;

class OperatorChain
{
    /**
     * @var OperatorInterface[]|iterable
     */
    protected $operators;

    /**
     * OperatorChain constructor.
     * @param iterable|OperatorInterface[] $operators
     */
    public function __construct(iterable $operators)
    {
        $this->operators = $operators;
    }

    /**
     * @return iterable|OperatorInterface[]
     */
    public function getOperators(): iterable
    {
        return $this->operators;
    }

    /**
     * @param float $firstNumber
     * @param float $secondNumber
     * @param string $operatorName
     *
     * @return float
     *
     * @throws OperatorNotFoundException
     * @throws CalculationException
     */
    public function calculate(float $firstNumber, float $secondNumber, string $operatorName): float
    {
        foreach ($this->operators as $operator) {
            if ($operator->getName() === $operatorName) {
                return $operator->calculate($firstNumber, $secondNumber);
            }
        }

        throw new OperatorNotFoundException(sprintf('Operator "%s" not found.', $operatorName));
    }
}
