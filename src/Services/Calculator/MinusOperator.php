<?php

namespace App\Services\Calculator;

class MinusOperator implements OperatorInterface
{
    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return 'minus';
    }

    /**
     * @inheritDoc
     */
    public function calculate(float $firstNumber, float $secondNumber): float
    {
        return $firstNumber - $secondNumber;
    }
}
