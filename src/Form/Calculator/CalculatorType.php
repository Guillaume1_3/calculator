<?php


namespace App\Form\Calculator;

use App\Domain\Command\CalculateCommand;
use App\Services\Calculator\DivisionOperator;
use App\Services\Calculator\OperatorChain;
use App\Services\Calculator\OperatorInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CalculatorType extends AbstractType
{
    /**
     * @var OperatorChain
     */
    protected $operatorChain;

    /**
     * CalculatorType constructor.
     *
     * @param OperatorChain $operatorChain
     */
    public function __construct(OperatorChain $operatorChain)
    {
        $this->operatorChain = $operatorChain;
    }

    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstNumber', NumberType::class)
            ->add('operator', ChoiceType::class, [
                'choices' => $this->operatorChain->getOperators(),
                'choice_label' => function (OperatorInterface $operator) {
                    return sprintf('operator.%s.symbol', $operator->getName());
                },
                'choice_translation_domain' => 'calculator',
                'choice_value' => 'name',
            ])
            ->add('secondNumber', NumberType::class)
        ;
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
           'data' => CalculateCommand::class,
            'validation_groups' => function (FormInterface $form) {
                $groups = ['Default'];

                if ($form->getData()->getOperator() && $form->getData()->getOperator() instanceof DivisionOperator) {
                    $groups[] = 'division_operator';
                }

                return $groups;
            }
        ]);
    }
}
