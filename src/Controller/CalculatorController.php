<?php

namespace App\Controller;

use App\Domain\Command\CalculateCommand;
use App\Domain\Command\CalculateHandler;
use App\Form\Calculator\CalculatorType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CalculatorController
 *
 * @Route("/calculator")
 */
class CalculatorController extends AbstractController
{
    /**
     * @Route(path="/", name="calculator")
     *
     * @param Request $request
     * @param FormFactoryInterface $formFactory
     * @param CalculateHandler $calculateHandler
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \App\Exception\Calculator\CalculationException
     * @throws \App\Exception\Calculator\OperatorNotFoundException
     */
    public function calculate(Request $request, FormFactoryInterface $formFactory, CalculateHandler $calculateHandler)
    {
        $calculateCommand = new CalculateCommand();

        $calculatorForm = $formFactory->create(CalculatorType::class, $calculateCommand);

        $calculatorForm->handleRequest($request);

        if ($calculatorForm->isSubmitted() && $calculatorForm->isValid()) {
            $result = $calculateHandler->handle($calculateCommand);
        }

        return $this->render(
            'calculator/calculator.html.Twig',
            [
                'calculator_form' => $calculatorForm->createView(),
                'result' => $result ?? null,
            ]
        );
    }
}
