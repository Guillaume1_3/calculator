<?php

namespace App\Exception\Calculator;

class OperatorNotFoundException extends CalculationException
{
}
