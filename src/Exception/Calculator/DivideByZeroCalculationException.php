<?php

namespace App\Exception\Calculator;

class DivideByZeroCalculationException extends CalculationException
{
}
