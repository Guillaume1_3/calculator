<?php

namespace App\Domain\Command;

use App\Services\Calculator\OperatorInterface;
use Symfony\Component\Validator\Constraints as Assert;

class CalculateCommand
{
    /**
     * @var float
     *
     * @Assert\NotBlank
     */
    protected $firstNumber;

    /**
     * @var float
     *
     * @Assert\NotBlank
     * @Assert\NotEqualTo(value=0, groups={"division_operator"})
     */
    protected $secondNumber;

    /**
     * @var OperatorInterface
     */
    protected $operator;

    /**
     * @return float
     */
    public function getFirstNumber()
    {
        return $this->firstNumber;
    }

    /**
     * @param float $firstNumber
     *
     * @return CalculateCommand
     */
    public function setFirstNumber($firstNumber)
    {
        $this->firstNumber = $firstNumber;

        return $this;
    }

    /**
     * @return float
     */
    public function getSecondNumber()
    {
        return $this->secondNumber;
    }

    /**
     * @param float $secondNumber
     *
     * @return CalculateCommand
     */
    public function setSecondNumber($secondNumber)
    {
        $this->secondNumber = $secondNumber;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getOperator()
    {
        return $this->operator;
    }

    /**
     * @param mixed $operator
     *
     * @return CalculateCommand
     */
    public function setOperator(OperatorInterface $operator)
    {
        $this->operator = $operator;

        return $this;
    }
}
