<?php

namespace App\Domain\Command;

use App\Services\Calculator\OperatorChain;

class CalculateHandler
{
    /**
     * @var OperatorChain
     */
    protected $operatorChain;

    /**
     * CalculateHandler constructor.
     *
     * @param OperatorChain $operatorChain
     */
    public function __construct(OperatorChain $operatorChain)
    {
        $this->operatorChain = $operatorChain;
    }

    /**
     * @param CalculateCommand $calculateCommand
     *
     * @return float
     *
     * @throws \App\Exception\Calculator\CalculationException
     * @throws \App\Exception\Calculator\OperatorNotFoundException
     */
    public function handle(CalculateCommand $calculateCommand)
    {
        return $this->operatorChain->calculate(
            $calculateCommand->getFirstNumber(),
            $calculateCommand->getSecondNumber(),
            $calculateCommand->getOperator()->getName()
        );
    }
}
