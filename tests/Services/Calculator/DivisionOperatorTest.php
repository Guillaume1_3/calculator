<?php

namespace App\Tests\Services\Calculator;

use App\Exception\Calculator\DivideByZeroCalculationException;
use App\Services\Calculator\DivisionOperator;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class DivisionOperatorTest extends KernelTestCase
{
    public function testDivideByZero()
    {
        $this->expectException(DivideByZeroCalculationException::class);

        $operator = new DivisionOperator();

        $operator->calculate(1, 0);
    }

    public function testDivideByZeroDotSomething()
    {
        $operator = new DivisionOperator();

        $this->assertEquals(10, $operator->calculate(1, 0.1));
    }
}
