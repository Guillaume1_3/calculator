<?php

namespace App\Tests\Services\Calculator;

use App\Exception\Calculator\OperatorNotFoundException;
use App\Services\Calculator\PlusOperator;
use App\Services\Calculator\OperatorChain;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class OperatorChainTest extends KernelTestCase
{
    public function testIfOperatorExists()
    {
        $operator = new PlusOperator();

        $chain = new OperatorChain([$operator]);

        $this->assertEquals(2, $chain->calculate(1,1, $operator->getName()));
    }


    public function testOperatorNotFound()
    {
        $this->expectException(OperatorNotFoundException::class);

        $operator = new PlusOperator();
        $chain = new OperatorChain([$operator]);

        $chain->calculate(1,1, 'foobar');
    }
}
